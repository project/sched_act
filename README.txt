Scheduled Actions module

PREREQUISITES

- Drupal 4.7
- Actions module
- cron

OVERVIEW

The Scheduled Actions module provides support for scheduling and
managing Actions to be invoked in the future.

INSTALLATION

1.  Install and activate Scheduled Actions like every other Drupal module.
2.  Make sure http://yoursite/cron.php is invoked on a regular basis.

Scheduled Actions is not useful unless you also have Actions installed
and, quite probably, unless you are using Workflow.

DESCRIPTION

Action-scheduling actions

Scheduled Actions defines an Action template called "Create an
action-scheduling action".  An action-scheduling action (ASA)
schedules some other action to occur in the future. For example, you
could created an ASA to execute the 'Publish a node' action in 3 days
(you might name this ASA "Publish Node in 3 days").  You could then
assign this action to the (creation) transition in a workflow from the
Workflow module.  When a new node mananged by that workflow is
created, the 'Publish a node' action will be scheduled and, three
days later, the node will be published.

ASAs can be instantiated with the following options:

- Action to schedule, such as "Publish Node".

- When to schedule it, including a number and a unit, such as "2
  weekdays".  The unit can be seconds, minutes, hours, days, or
  weekdays.

- Argument Types, which specifies the type for up to four run-time
  arguments to the Action.  This will almost always be type 1 node and
  types 2-4 undefined.

- Filter.  This restricts the ASA only to operate on nodes with at
  least one of the selected vocabulary terms, doing nothing for all
  others.

Managing scheduled actions

Scheduled actions can be managed in two places.  The administer >>
actions >> scheduled actions menu displays a list of all scheduled
actions in the system (with links to the affected node(s)), and each
node's edit form displays a list of all scheduled actions that apply
to that node.

In either list of scheduled actions, each row of the list shows which
action is scheduled, when it is scheduled to occur, and when it
actually did occur if it already has.  It also includes a checkbox to
delete the scheduled action.  An action's scheduled date can be edited
via the Scheduled column. Once an action is performed, the Scheduled
column is no longer editable.

Executing scheduled actions

Scheduled actions are executed during the first invocation of cron.php
that occurs after the action's scheduled date.  Once a scheduled
action is executed, its actual execution time is displayed next to its
scheduled time.

AUTHOR

Barry Jaspan
firstname at lastname dot org

SPONSOR

WheresSpot, Inc.
www.wheresspot.com
